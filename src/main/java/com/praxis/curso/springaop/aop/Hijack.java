package com.praxis.curso.springaop.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Hijack {


	@Pointcut("execution(* com.praxis.curso.springaop.service.*.print*(..))")
	public void hijackForAllMethods(){}

	@Before("hijackForAllMethods()")
	public void beforeAdvice(JoinPoint jp) throws Throwable {
        System.out.println("hijacked beforeAdvice: " + jp.getSignature().getName());
    }

	@After("hijackForAllMethods()")
	public void afterAdvice(JoinPoint jp) throws Throwable {
        System.out.println("hijacked afterAdvice: " + jp.getSignature().getName());
    }

	@Around("hijackForAllMethods()")
	public Object aroundAdvice(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("hijacked aroundAdvice::ANTES: " + pjp.getSignature().getName());
    	Object valorRetorno = pjp.proceed();
    	System.out.println("hijacked aroundAdvice::DESPUES");
    	return valorRetorno;
    }

	@AfterReturning( pointcut="hijackForAllMethods()", returning="valor")
	public void afterReturningAdvice(Object valor) {
    	System.out.println("hijacked afterReturningAdvice: " + valor);
	}
	
	@AfterThrowing( pointcut="hijackForAllMethods()", throwing="exc")
	public void afterThrowingAdvice(IllegalArgumentException exc) {
		System.out.println("hijacked afterThrowingAdvice: " + exc);
	}

	@After("execution(* com.praxis.curso.springaop.service.*.printURL*(..))")
	public void afterAdviceForPrintURL(JoinPoint jp) throws Throwable {
        System.out.println("hijacked printURL: " + jp.getSignature().getName());
    }
	

}
