package com.praxis.curso.springaop.service;

public interface CustomerService {

	public void printName();
	
	public void printURL();

	public void printThrowException();

}
