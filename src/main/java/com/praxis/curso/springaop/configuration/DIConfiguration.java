package com.praxis.curso.springaop.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.praxis.curso.springaop.aop.Hijack;
import com.praxis.curso.springaop.service.CustomerService;
import com.praxis.curso.springaop.service.CustomerServiceImpl;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan( basePackages = {
		"com.praxis.curso.springaop.main"
	})
public class DIConfiguration {

	@Bean
	public CustomerService getCustomerService(){
		CustomerServiceImpl customerService = new CustomerServiceImpl();
		customerService.setName("GOVX");
		customerService.setUrl("http://govx.praxis.com.mx");
		return customerService;
	}
	
	@Bean
	public Hijack getHijack() {
		return new Hijack();
	}
	
}
