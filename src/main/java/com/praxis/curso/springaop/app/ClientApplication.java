package com.praxis.curso.springaop.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.praxis.curso.springaop.configuration.DIConfiguration;
import com.praxis.curso.springaop.main.MyApplication;

public class ClientApplication {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DIConfiguration.class);
		MyApplication app = context.getBean(MyApplication.class);

		app.doStuff();
		
		context.close();
	}

}
