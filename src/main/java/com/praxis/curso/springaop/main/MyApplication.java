package com.praxis.curso.springaop.main;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import com.praxis.curso.springaop.service.CustomerService;

@Component
public class MyApplication {

	private CustomerService customerService;
	
	@Autowired
	public void setCustomerService(CustomerService customerService){
		this.customerService = customerService;
	}
	
	public void doStuff(){
		
		System.out.println("****************************************************************************************************");
		System.out.println("****************************************************************************************************");
		this.customerService.printName();
		System.out.println("****************************************************************************************************");
		System.out.println("****************************************************************************************************");
		this.customerService.printURL();
		System.out.println("****************************************************************************************************");
		System.out.println("****************************************************************************************************");
		try {
			this.customerService.printThrowException();
		} catch (Exception e) {
			System.out.println( "Error: " + e.getMessage() );
		}
		System.out.println("****************************************************************************************************");
		System.out.println("****************************************************************************************************");

	}
}
